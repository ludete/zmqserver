package com.ludete.zmqtest;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.google.common.primitives.Ints;

public class ZmqServer{
    public static void main (String[] args) {
        Context context = ZMQ.context(1);

        //  Socket to talk to clients
        Socket responder  = context.socket(ZMQ.PUB);
        responder.bind("tcp://*:5567");

        System.out.println("launch and connect server.");

        int success = 0;
        int failed = 0;
        while (true) {
            //  Send reply back to client
            boolean ifSucceed = responder.send("-1hello");
            if (ifSucceed){
                System.out.println("send message success " + success++);
            }else {
                System.out.println("send message success " + failed++);
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        //  We never get here but clean up anyhow
    }
}

